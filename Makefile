install:
	if [ ! -f /etc/os-release ]; then \
	  make mac; \
	else \
	  make manjaro; \
	fi

PACMAN-PKGS = make/manjaro-pkgs.mk
PKGS != cat $(PACMAN-PKGS)
	
manjaro:
	git submodule update --init --recursive
	sudo pacman -Syyu --noconfirm
	mkdir -p ~/git/personal ~/git/nebulaworks
	sudo pacman -S $(PKGS) --noconfirm
	make ln-linux

BREW-CLI = make/brew-cli.mk
CLI != cat $(BREW-CLI)
	
BREW-CASK = make/brew-cask.mk
CASK != cat $(BREW-CASK)

MAC-SETUP = make/mac-setup.mk
SETUP != cat $(MAC-SETUP)

mac:
	git submodule update --init --recursive
	/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
	brew install $(CLI)
	brew cask install $(CASK)
	open $(HOME)/.dotfiles/term-tpl.terminal
	open $(HOME)/.dotfiles/Campbell.bttpreset
	mkdir $(HOME)/.config/karabiner
	make ln-mac
	$(SETUP)

LINUX-CONFIG = .bash_profile .tmux.conf .vim .vimrc .zshrc .git_plugin .gitconfig* .alias .Xresources

ln-linux:
	for config in $(LINUX-CONFIG); do \
	  rm -rf $(HOME)/$$config; \
	  ln -s $(HOME)/.dotfiles/$$config $(HOME); \
	done
	ln -s $(HOME)/.dotfiles/ohmyzsh $(HOME)/.oh-my-zsh
	ln -s $(HOME)/.dotfiles/jcampbell.zsh-theme $(HOME)/.oh-my-zsh/themes/$$(whoami).zsh-theme
	rm -rf $(HOME)/.config
	ln -s $(HOME)/.dotfiles/config $(HOME)/.config
	sudo ln -s ~/.dotfiles/SanFranciscoFont/*.otf /usr/share/fonts/OTF/

MAC-CONFIG = .bash_profile .tmux.conf .vim .vimrc .zshrc .git_plugin .gitconfig* .alias

ln-mac:
	for config in $(MAC-CONFIG); do \
	  rm -rf $(HOME)/$$config; \
	  ln -s $(HOME)/.dotfiles/$$config $(HOME); \
	done
	ln -s $(HOME)/.dotfiles/ohmyzsh $(HOME)/.oh-my-zsh
	ln -s $HOME/.dotfiles/jcampbell.zsh-theme $HOME/.oh-my-zsh/themes/$(whoami).zsh-theme
	ln -s $HOME/.dotfiles/config/karabiner/karabiner.json $HOME/.config/karabiner/karabiner.json
