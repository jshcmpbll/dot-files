###############################################################################
###############################################################################
### SYSTEM CONFIGURATION    (Config origin found at .macos in current dir ) ###
###############################################################################
###############################################################################

###############################################################################
# General UI/UX                                                               #
###############################################################################

# Set highlight color to graphite
defaults write NSGlobalDomain AppleHighlightColor -string "0.847059 0.847059 0.862745"

# Set accent color to gray
defaults write NSGlobalDomain AppleAccentColor -string "-1"





###############################################################################
# Trackpad, mouse, keyboard, Bluetooth accessories, and input                 #
###############################################################################

# Trackpad: enable tap to click for this user and for the login screen
defaults write com.apple.driver.AppleBluetoothMultitouch.trackpad Clicking -bool true
defaults -currentHost write NSGlobalDomain com.apple.mouse.tapBehavior -int 1
defaults write NSGlobalDomain com.apple.mouse.tapBehavior -int 1

# Trackpad: map bottom right corner to right-click
defaults write com.apple.driver.AppleBluetoothMultitouch.trackpad TrackpadCornerSecondaryClick -int 2
defaults write com.apple.driver.AppleBluetoothMultitouch.trackpad TrackpadRightClick -bool true
defaults -currentHost write NSGlobalDomain com.apple.trackpad.trackpadCornerClickBehavior -int 1
defaults -currentHost write NSGlobalDomain com.apple.trackpad.enableSecondaryClick -bool true

# If Mac is Mobile, turn on battery percent
# Eventually this command will check if the computer uses a batery using this command 'pmset -g batt'

defaults write com.apple.menuextra.battery ShowPercent YES

open ~/.dotfiles/term-tpl.terminal

default write -app Terminal "Default Window Settings" "term-tpl"
default write -app Terminal "Startup Window Settings" "term-tpl"
